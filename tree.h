#ifndef TREE_H
#define TREE_H

#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

namespace Tree {
    using std::stringstream;
    using std::string;
    using std::vector;
    using std::fstream;

    typedef char instruction[3];
    const instruction startTree("sT");
    const instruction endTree("eT");
    bool is_equal(const instruction a, const instruction b) {
        return !strncmp(a, b, 3);
    }

    template <class T>
    class Tree {
        private:
            T root;
            vector<Tree<T>> children;

            void _serialize(fstream& file) const noexcept {
                file.write(startTree, sizeof(instruction));
                file.write((char*) &root, sizeof(T));
                if(!isLeaf())
                    for(const Tree<T>& c: children)
                        c._serialize(file);
                file.write(endTree, sizeof(instruction));
            }
        public:
            Tree(T root) noexcept : root(root) {}

            Tree() noexcept {}

            Tree<T>& addChild(const T& c) noexcept {
                children.push_back(Tree<T>(c));
                return children.back();
            }

            #ifndef NDEBUG
                stringstream stream() const noexcept {
                stringstream toReturn;
                toReturn << get();
                if(!isLeaf()) {
                    toReturn << "{";
                    if(children.size() > 1)
                        for(std::size_t i(0); i < children.size() - 1; i++)
                            toReturn << children[i].stream().str() << ", ";
                    toReturn << children[children.size() - 1].stream().str() << "}";
                }
                return toReturn;
            }
            #endif

            T get() const noexcept {
                return root;
            }
            
            void set(const T& value) noexcept {
                root = value;
            }

            Tree<T>& getElement(const T& searchValue) {
                for(Tree<T>& c: children)
                    if(c.get() == searchValue)
                        return c;
                for(Tree<T>& c: children) {
                    if(c.hasElement(searchValue))
                        return c.getElement(searchValue);
                }
                throw "no such element.";
            }

            inline bool isLeaf() const noexcept {
                return children.empty();
            }

            bool hasElement(const T& searchValue) const noexcept {
                if(get() == searchValue)
                    return true;
                if(!isLeaf()) {
                    bool flag(false);
                    for(const Tree<T>& c: children)
                        flag |= c.hasElement(searchValue);
                    return flag;
                }
                return false;
            }

            void serialize(const string& location = "tree.tree") const noexcept {
                fstream file(location, std::ios::binary | std::ios::out);
                _serialize(file);
            }

            void deserialize(const string& location = "tree.tree") noexcept {
                fstream file(location, std::ios::binary | std::ios::in);
                if(!file.is_open()) {
                    serialize(location);
                    file.open(location, std::ios::binary | std::ios::in);
                }
                instruction temp;
                vector<Tree<T>*> pathStack;
                do {
                    file.read(temp, sizeof(instruction));
                    if(is_equal(temp, startTree)) {
                        if(pathStack.empty())
                            pathStack.push_back(this);
                        else {
                            pathStack.back()->children.push_back(Tree<T>());
                            pathStack.push_back(&(pathStack.back()->children.back()));
                        }
                        file.read((char*) &(pathStack.back()->root), sizeof(T));
                        continue;
                    }
                    if(is_equal(temp, endTree))
                        pathStack.pop_back();
                } while(!pathStack.empty());
            }

            bool isEmpty() const noexcept {
                return (root == T());
            }

            vector<T> getChildren() const noexcept {
                vector<T> toReturn;
                for(const auto& c: children)
                    toReturn.push_back(c.get());
                return toReturn;
            }
            size_t childrenCount() const noexcept {
                return children.size();
            }
    };

    template<>
    void Tree<string>::_serialize(fstream& file) const noexcept {
        file.write(startTree, sizeof(instruction));
        size_t length = root.length() + 1;
        file.write((char*) &length, sizeof(size_t));
        file.write(root.c_str(), length * sizeof(char));
        if(!isLeaf())
            for(const Tree<string>& c: children)
                c._serialize(file);
        file.write(endTree, sizeof(instruction));
    }

    template<>
    void Tree<string>::deserialize(const string& location) noexcept {
        fstream file(location, std::ios::binary | std::ios::in);
        if(!file.is_open()) {
            serialize(location);
            file.open(location, std::ios::binary | std::ios::in);
        }
        instruction temp;
        vector<Tree<string>*> pathStack;
        do {
            file.read(temp, sizeof(instruction));
            if(is_equal(temp, startTree)) {
                if(pathStack.empty())
                    pathStack.push_back(this);
                else {
                    pathStack.back()->children.push_back(Tree<string>());
                    pathStack.push_back(&(pathStack.back()->children.back()));
                }
                size_t length;
                file.read((char*) &length, sizeof(size_t));
                char* c_string = new char[length];
                file.read(c_string, length);
                pathStack.back()->root = string(c_string);
                delete[] c_string;
                continue;
            }
            if(is_equal(temp, endTree))
                pathStack.pop_back();
        } while(!pathStack.empty());
    }
}

#endif //TREE_H