# Serializable Tree

## Description
This simple serializable tree is made up as a template and
supports all the inbuilt data types of c++ along with a specialization for `std::string`s.

## Installation
Download the `tree.h` file and place it into your project directory. Add `#include "tree.h"` along with your other `#include`s. You can head over to the Wiki to see its usage.

## Contributing
Anyone is welcome to contribute to my code. Raise issues, send pull requests, anything on your mind! Some basic function-wise contribution notes are given in the wiki.